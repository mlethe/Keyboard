package com.mlethe.library.keyboard;

import android.content.Context;
import android.widget.EditText;

import androidx.annotation.Nullable;

/**
 * 符号键盘
 * @author Mlethe
 */
public class SymbolKeyboard extends Keyboard {
    public SymbolKeyboard(Context context) {
        super(context, R.xml.keyboard_symbol);
    }

    @Override
    protected boolean onKey(int primaryCode, @Nullable EditText editText) {
        if (primaryCode == -2) {
            // 数字切换
            switchKeyboard(NumKeyboard.class);
            return true;
        } else if (primaryCode == 90001) {
            // 字母切换
            switchKeyboard(WordKeyboard.class);
            return true;
        }
        return false;
    }
}
