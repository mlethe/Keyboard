package com.mlethe.library.keyboard;

import android.content.Context;
import android.widget.EditText;

import androidx.annotation.Nullable;

/**
 * 数字键盘
 * @author Mlethe
 */
public class NumKeyboard extends Keyboard {

    public NumKeyboard(Context context) {
        super(context, R.xml.keyboard_numbers);
    }

    @Override
    protected boolean onKey(int primaryCode, @Nullable EditText editText) {
        if (primaryCode == -2) {
            // 字母切换
            switchKeyboard(WordKeyboard.class);
            return true;
        }
        return false;
    }

    @Override
    public boolean isRandom() {
        return true;
    }
}
