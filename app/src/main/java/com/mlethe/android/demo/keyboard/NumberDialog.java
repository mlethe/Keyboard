package com.mlethe.android.demo.keyboard;

import android.app.Activity;
import android.view.View;

import com.mlethe.library.keyboard.Keyboard;
import com.mlethe.library.keyboard.KeyboardWindow;

/**
 * @author Mlethe
 * @date 2021/3/19
 */
public class NumberDialog extends KeyboardWindow {
    public NumberDialog(Activity activity) {
        super(activity);
    }

    @Override
    protected void onViewCreated(View view) {
        setNavigationHide(true);
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_keyboard_numbers;
    }

    @Override
    protected int getKeyboardId() {
        return R.id.keyboard_view;
    }

    @Override
    protected Keyboard[] getKeyboards() {
        return new Keyboard[]{new Keyboard(getContext(), R.xml.keyboard_numbers)};
    }
}
