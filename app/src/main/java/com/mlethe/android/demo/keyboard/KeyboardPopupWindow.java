package com.mlethe.android.demo.keyboard;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import com.mlethe.library.keyboard.Keyboard;
import com.mlethe.library.keyboard.KeyboardWindow;
import com.mlethe.library.keyboard.NumKeyboard;
import com.mlethe.library.keyboard.SymbolKeyboard;
import com.mlethe.library.keyboard.WordKeyboard;

public class KeyboardPopupWindow extends KeyboardWindow {

    public KeyboardPopupWindow(Activity activity) {
        super(activity);
    }

    @Override
    protected int getLayout() {
        return R.layout.keyboard_key_board_popu;
    }

    @Override
    protected void onViewCreated(View view) {
        view.findViewById(R.id.keyboard_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        getKeyboardView().setOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                Log.e("yym", "onViewDetachedFromWindow: " + (getEditText() != null));
            }
        });
    }

    @Override
    protected int getKeyboardId() {
        return R.id.keyboard_view;
    }

    @Override
    protected Keyboard[] getKeyboards() {
        return new Keyboard[] {new WordKeyboard(getContext()), new NumKeyboard(getContext()), new SymbolKeyboard(getContext())};
    }
}
