package com.mlethe.android.demo.keyboard;

import android.content.Context;

import com.mlethe.library.keyboard.Keyboard;

public class PlateHaveChineseKeyboard extends Keyboard {
    public PlateHaveChineseKeyboard(Context context) {
        super(context, R.xml.keyboard_plate_have_chinese);
    }

    @Override
    protected boolean getPreviewEnabled(int primaryCode) {
        return true;
    }
}
