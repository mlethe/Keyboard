package com.mlethe.android.demo.keyboard;

import android.content.Context;

import com.mlethe.library.keyboard.Keyboard;

public class PlateKeyboard extends Keyboard {
    public PlateKeyboard(Context context) {
        super(context, R.xml.keyboard_plate);
    }

    @Override
    protected boolean getPreviewEnabled(int primaryCode) {
        return true;
    }
}
