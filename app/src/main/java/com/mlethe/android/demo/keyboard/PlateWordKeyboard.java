package com.mlethe.android.demo.keyboard;

import android.content.Context;

import com.mlethe.library.keyboard.Keyboard;

public class PlateWordKeyboard extends Keyboard {
    public PlateWordKeyboard(Context context) {
        super(context, R.xml.keyboard_plate_word);
    }

    @Override
    protected boolean getPreviewEnabled(int primaryCode) {
        return true;
    }
}
