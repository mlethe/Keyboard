package com.mlethe.android.demo.keyboard;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.mlethe.library.keyboard.KeyboardView;
import com.mlethe.library.widget.verifycode.VerifyCodeView;

public class MainActivity extends AppCompatActivity {

    private KeyboardView keyboardView;
    private NumberDialog keyboardPopupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final VerifyCodeView firstVcv = findViewById(R.id.verify_code_first);
        final EditText secondEt = findViewById(R.id.second_et);
        final View testView = findViewById(R.id.testView);
        final LinearLayout keyboardLayout = findViewById(R.id.keyboard_layout);
        keyboardView = findViewById(R.id.plate_number_keyboard_view);
        keyboardView.setKeyboards(new PlateKeyboard(getApplicationContext()), new PlateWordKeyboard(getApplicationContext()), new PlateHaveChineseKeyboard(getApplicationContext()), new PlateWithoutChineseKeyboard(getApplicationContext()))
                .setPreviewEnabled(false);
        firstVcv.setOnVerifyCodeListener(new VerifyCodeView.OnVerifyCodeListener() {
            @Override
            public void onFocusChange(VerifyCodeView.EditText editText, boolean hasFocus) {
                if (hasFocus) {
                    keyboardView.showKeyboard(editText);
                } else {
                    keyboardView.hiddenKeyboard();
                }
            }

            @Override
            public void onChanged(VerifyCodeView.EditText editText, int index) {
                if (index == 0) {
                    keyboardView.switchKeyboard(PlateKeyboard.class);
                } else if (index == 1){
                    keyboardView.switchKeyboard(PlateWordKeyboard.class);
                } else if (index == 6) {
                    keyboardView.switchKeyboard(PlateHaveChineseKeyboard.class);
                } else {
                    keyboardView.switchKeyboard(PlateWithoutChineseKeyboard.class);
                }
            }

            @Override
            public void onComplete(String text) {

            }
        });
        secondEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (keyboardPopupWindow == null) {
                    keyboardPopupWindow = new NumberDialog(MainActivity.this);
                    keyboardPopupWindow.addTouchInterceptorView(testView)
                            .setEditText(secondEt)
                            .setNavigationHide(true);
                }
                keyboardPopupWindow.show();
            }
        });
        secondEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                Log.e("yym", "onFocusChange: " + b);
                if (b) {
                    if (keyboardPopupWindow == null) {
                        keyboardPopupWindow = new NumberDialog(MainActivity.this);
                        keyboardPopupWindow.addTouchInterceptorView(testView)
                                .setEditText(secondEt)
                                .setNavigationHide(true);
                    }
                    keyboardPopupWindow.show();
                } else {
                    if (keyboardPopupWindow != null) {
                        keyboardPopupWindow.dismiss();
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (keyboardPopupWindow != null) {
            keyboardPopupWindow.dismiss();
            keyboardPopupWindow = null;
        }
        super.onDestroy();
    }
}