package com.mlethe.android.demo.keyboard;

import android.content.Context;

import com.mlethe.library.keyboard.Keyboard;

public class PlateWithoutChineseKeyboard extends Keyboard {
    public PlateWithoutChineseKeyboard(Context context) {
        super(context, R.xml.keyboard_plate_without_chinese);
    }

    @Override
    protected boolean getPreviewEnabled(int primaryCode) {
        return true;
    }
}
